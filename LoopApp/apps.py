from django.apps import AppConfig


class LoopappConfig(AppConfig):
    name = 'LoopApp'
