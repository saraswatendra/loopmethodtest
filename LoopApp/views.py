from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView
from .serializers import *
from .models import *
from django.core.files.base import ContentFile
import base64
from django.db.models import Q
from django.contrib.auth import authenticate,login,logout


#-----------------------------API for Register User --------------------------------------------------

class CreateProfile(CreateAPIView):
    def create(self,request):
        params = request.data
        try:
        	if params['permanent_address'] != '':
        		permanent_add_serializer = AddressSerializer(data=params['permanent_address'])
	        	permanent_add_serializer.is_valid(raise_exception=True)
	        	perm_address = permanent_add_serializer.save()
	        	params['permanent_address'] = perm_address.id

	        if params['company_address'] != '':
        		company_add_serializer = AddressSerializer(data=params['company_address'])
	        	company_add_serializer.is_valid(raise_exception=True)
	        	comp_address = company_add_serializer.save()
	        	params['company_address'] = comp_address.id

        	if 'profile_pic' in params:
        		params['profile_pic'] = ContentFile(base64.b64decode(str.encode(params.get('profile_pic'))), name=params.get('email')+'.png')
        	serializer = ProfileSerializer(data=params)
        	serializer.is_valid(raise_exception=True)
        	user = serializer.save()
        	user.set_password(params['password'])
        	user.save()
        	return Response({"data":serializer.data, "message": "Signed up successfully."},status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)            
            return Response(serializer.errors)


#-----------------------------API for User Login--------------------------------------------------

class UserLogin(APIView):
    def post(self,request):
        params = request.data
        try:
            user_exist = Profile.objects.get(email=params['email'])
            user = authenticate(email=params['email'], password=params['password'])
            if user and not user.is_superuser:
                serializer = ProfileSerializer(user,context={"request":request})
                login(request,user)                    
                return Response({"message":"Logged in successfully.","data":serializer.data,"token":user.create_jwt()},status=status.HTTP_200_OK)

            else:
                return Response({"message":"Please enter correct credentials"},status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response({"message":"Email not registered."},status=status.HTTP_404_NOT_FOUND)	


#-----------------------------API for User Logout--------------------------------------------------

class UserLogout(APIView):
	permission_classes = (IsAuthenticated,)
	def get(self,request):
		logout(request)
		return Response({"message": "Logged out successfully"},status=status.HTTP_200_OK)



#-----------------------------API for User Profile--------------------------------------------------

class UserProfile(APIView):
	permission_classes = (IsAuthenticated,)
	def get(self,request):
		try:
			user = Profile.objects.get(email=request.user.email)
			if user and not user.is_superuser:
				serializer = ProfileDataSerializer(user,context={"request":request})
				return Response({"message":"Profile Data.","data":serializer.data},status=status.HTTP_200_OK)
			else:
				return Response({"message":"You are not authorized to view this data"},status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({"message":"Profile not found."},status=status.HTTP_404_NOT_FOUND)

	def patch(self,request):
		params = request.data
		try:
			user = Profile.objects.get(email=request.user.email) 
			if user and not user.is_superuser:
				if params['permanent_address'] != '':
					address = Address.objects.filter(id=user.permanent_address.id).first()
					
					if address:
						permanent_add_serializer = AddressSerializer(address, data=params['permanent_address'], partial=True)
						permanent_add_serializer.is_valid(raise_exception=True)						
						per_add = permanent_add_serializer.save()
						params['permanent_address'] = per_add.id
						
					else:
						permanent_add_serializer = AddressSerializer(data=params['permanent_address'])
						permanent_add_serializer.is_valid(raise_exception=True)
						per_add = permanent_add_serializer.save()
						params['permanent_address'] = per_add.id

				if params['company_address'] != '':
					address = Address.objects.filter(id=user.company_address.id).first()
					if address:
						company_add_serializer = AddressSerializer(address, data=params['company_address'], partial=True)
						company_add_serializer.is_valid(raise_exception=True)
						com_add = company_add_serializer.save()
						params['company_address'] = com_add.id
					else:
						company_add_serializer = AddressSerializer(data=params['company_address'])
						company_add_serializer.is_valid(raise_exception=True)
						com_add = company_add_serializer.save()
						params['company_address'] = com_add.id


				if 'profile_pic' in params:
					params['profile_pic'] = ContentFile(base64.b64decode(str.encode(params.get('profile_pic'))), name=request.user.email+'.png')
				serializer = ProfileDataSerializer(user,context={"request":request},data=params,partial=True)
				
				serializer.is_valid(raise_exception=True)
				
				serializer.save()
				
				return Response({"message":"Profile Updated Successfully.","data":serializer.data},status=status.HTTP_200_OK)
			else:
				return Response({"message":"You are not authorized to update this data"},status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({"message":"Profile not found."},status=status.HTTP_404_NOT_FOUND)	


class UserProfileList(APIView):
	permission_classes = (IsAuthenticated,)
	def get(self,request):
		try:
			users = Profile.objects.filter(is_superuser=False).exclude(email=request.user.email)
			serializer = ProfileListSerializer(users,context={"request":request}, many=True)
			return Response({"message":"Users Profile List Data.","data":serializer.data},status=status.HTTP_200_OK)
		except Exception as e:
			print(e)
			return Response({"message":"Users Profile List not found."},status=status.HTTP_404_NOT_FOUND)

	def post(self,request):
		params = request.data
		try:
			if params['gender'] and params['permanent_city']:
				users = Profile.objects.filter(Q(is_superuser=False) & Q(gender=params['gender'].lower()) & Q( permanent_address__city__icontains=params['permanent_city'])).exclude(email=request.user.email)
			if params['gender'] and not params['permanent_city']:
				users = Profile.objects.filter(is_superuser=False, gender=params['gender'].lower()).exclude(email=request.user.email)
			if params['permanent_city'] and not params['gender']:
				users = Profile.objects.filter(is_superuser=False, permanent_address__city__icontains=params['permanent_city']).exclude(email=request.user.email)
			if not params['gender'] and not params['permanent_city']:
				users = Profile.objects.filter(is_superuser=False).exclude(email=request.user.email)

			serializer = ProfileListSerializer(users,context={"request":request}, many=True)
			return Response({"message":"Users Profile List Data.","data":serializer.data},status=status.HTTP_200_OK)
		except Exception as e:
			print(e)
			return Response({"message":"Users Profile List not found."},status=status.HTTP_404_NOT_FOUND)



class AddFriend(APIView):
	permission_classes = (IsAuthenticated,)	
	def post(self,request):
		params = request.data
		try:
			user = Profile.objects.get(email=request.user.email) 
			if user and not user.is_superuser:				
				user.friends.add(*params['friend_id'])		
				user.save()				
				return Response({"message":"Friend Added Successfully."},status=status.HTTP_200_OK)
			else:
				return Response({"message":"You are not authorized to update this data"},status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({"message":"Profile not found."},status=status.HTTP_404_NOT_FOUND)	


class RemoveFriend(APIView):
	permission_classes = (IsAuthenticated,)	
	def post(self,request):
		params = request.data
		try:
			user = Profile.objects.get(email=request.user.email) 
			if user and not user.is_superuser:				
				user.friends.remove(*params['friend_id'])
				user.save()
				return Response({"message":"Friend Removed Successfully."},status=status.HTTP_200_OK)
			else:
				return Response({"message":"You are not authorized to update this data"},status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({"message":"Profile not found."},status=status.HTTP_404_NOT_FOUND)	
