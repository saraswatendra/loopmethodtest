# Generated by Django 2.2.6 on 2019-10-02 11:58

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LoopApp', '0002_auto_20191002_1155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='friends',
            field=models.ManyToManyField(blank=True, null=True, related_name='_profile_friends_+', to=settings.AUTH_USER_MODEL),
        ),
    ]
