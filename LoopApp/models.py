from django.contrib.auth.models import BaseUserManager, AbstractBaseUser,PermissionsMixin
from django.db import models
from LoopMethod import settings
from rest_framework_jwt.utils import jwt_payload_handler
import jwt

class Address(models.Model):
	street_address = models.CharField("Street Address", max_length=255)
	city = models.CharField("city", max_length=20)
	state = models.CharField("state", max_length=20)
	pin_code = models.IntegerField("Pin Code")
	country = models.CharField("Country", max_length=20)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	

	def __str__(self):		
		return "%s" % (self.street_address)

	class Meta:
		'''docstring for meta'''
		verbose_name_plural="Address"	



class ProfileManager(BaseUserManager):
	''' Inherits BaseUserManager class'''
	def create_superuser(self,email, password):
		'''Creates and saves a superuser with the given email and password.'''
		user = self.model(email=email)
		user.set_password(password)
		user.is_superuser = True
		user.is_active = True
		user.is_staff = True
		user.save(using=self._db)
		return user


#--------------Model for website User--------------------

class Profile(AbstractBaseUser,PermissionsMixin):
	
	
	gender_choice = (
		('m','Male'),
		('f','Female'),
		('o','Other'),
		)
	name = models.CharField("Name", max_length=50)
	email = models.EmailField("Email", unique=True, max_length=50)
	phone_number = models.CharField("Phone Number", max_length=100, unique=True)
	gender = models.CharField("Gender",max_length=10, choices=gender_choice, blank=True, null=True)
	profile_pic = models.ImageField(upload_to="media")
	date_of_birth = models.DateField('Date of Birth', blank=True, null=True)
	permanent_address = models.OneToOneField(Address, on_delete=models.CASCADE, related_name= 'permanent_add', blank=True, null=True)
	company_address = models.OneToOneField(Address, on_delete=models.CASCADE, related_name= 'company_add', blank=True, null=True)
	friends = models.ManyToManyField("self", blank=True, null=True)	
	
	is_active = models.BooleanField('Active', default=True)	
	is_staff = models.BooleanField('Staff', default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)	

	objects = ProfileManager()
	USERNAME_FIELD = 'email'

	def __str__(self):
		return self.email

	def create_jwt(self):
		"""Function for creating JWT for Authentication Purpose"""
		payload = jwt_payload_handler(self)
		token = jwt.encode(payload, settings.SECRET_KEY)
		auth_token = token.decode('unicode_escape')
		return auth_token		

	class Meta:
		'''docstring for meta'''
		verbose_name_plural="Profile"
