from django.conf.urls import url
from .views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^register',CreateProfile.as_view()),
    url(r'^login',UserLogin.as_view()),
    url(r'^logout',UserLogout.as_view()),
    url(r'^profile$',UserProfile.as_view()),
    url(r'^profile-list$',UserProfileList.as_view()),
    url(r'^add-friend$',AddFriend.as_view()),
    url(r'^remove-friend$',RemoveFriend.as_view()),
    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


