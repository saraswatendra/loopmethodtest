from django.contrib import admin
from django.contrib.auth.models import Group
from .models import *
from django.utils.safestring import mark_safe

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	list_display=('email',)
	list_filter = ('gender','permanent_address__city','permanent_address__state','permanent_address__country')
	search_fields = ('name',)
	list_per_page = 10
	exclude = ('is_active','password','is_staff','groups','user_permissions','is_superuser')
	readonly_fields = ('Profile_Picture',)
	
	# def has_add_permission(self, request):
	# 	return False

	def formfield_for_manytomany(self, db_field, request, **kwargs):
		# print(db_field)
		kwargs["queryset"] = Profile.objects.filter(is_superuser=False)
		return super().formfield_for_manytomany(db_field, request, **kwargs)

	def Profile_Picture(self,obj):
		if obj.profile_pic:			
			return mark_safe("<img style='height:200px;width:200px;' src='{}' alt='user'></img>".format(obj.profile_pic.url))
		else:
			social_button = '<a  href="#">- - -</a>'
			return mark_safe(u''.join(social_button))
	

@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
	list_display=('city','country','state','pin_code')
	list_filter = ('city','country','state','pin_code')
	search_fields = ('city','country','state','pin_code')
	list_per_page = 10
	

admin.site.unregister(Group)