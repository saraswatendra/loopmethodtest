
from .models import *
from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField


class AddressSerializer(serializers.ModelSerializer):    
    class Meta:        
        model = Address
        fields = '__all__'

class ProfileSerializer(serializers.ModelSerializer):
    """Serializer for profile"""
    class Meta:        
        model = Profile
        fields = '__all__'

class ProfileDataSerializer(serializers.ModelSerializer):
	permanent_address = SerializerMethodField()
	def get_permanent_address(self, obj):
		if obj.permanent_address:			
			return {
				"street_address" : obj.permanent_address.street_address,
				"city" : obj.permanent_address.city,
				"state" : obj.permanent_address.state,
				"pin_code" : obj.permanent_address.pin_code,
				"country" : obj.permanent_address.country
				}
		return None
	company_address = SerializerMethodField()
	def get_company_address(self, obj):
		if obj.company_address:			
			return {
				"street_address" : obj.company_address.street_address,
				"city" : obj.company_address.city,
				"state" : obj.company_address.state,
				"pin_code" : obj.company_address.pin_code,
				"country" : obj.company_address.country
				}
		return None
	class Meta:        
		model = Profile
		fields = ('name','email','phone_number','gender','profile_pic','date_of_birth','permanent_address','company_address')


class ProfileListSerializer(serializers.ModelSerializer):
    
	is_my_friend = SerializerMethodField()
	def get_is_my_friend(self, obj):
		if obj:
			if self.context['request'].user in obj.friends.all():
				return "Yes"
			return "No"
		return None

	mutual_friend = SerializerMethodField()
	def get_mutual_friend(self, obj):		
		if obj:
			mutual_count = obj.friends.filter(id__in = self.context['request'].user.friends.all()).count()
			return mutual_count
		return None

	permanent_address = SerializerMethodField()
	def get_permanent_address(self, obj):
		if obj.permanent_address:			
			return {
				"street_address" : obj.permanent_address.street_address,
				"city" : obj.permanent_address.city,
				"state" : obj.permanent_address.state,
				"pin_code" : obj.permanent_address.pin_code,
				"country" : obj.permanent_address.country
				}
		return None

	class Meta:
		model = Profile
		fields = ('id','name','gender','profile_pic','permanent_address','is_my_friend','mutual_friend')


